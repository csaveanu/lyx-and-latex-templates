# A template for letters written in LyX and usable in LaTex as well

It looks like this when opened in the LyX editor:

<img src="./lyxtemplate_letter_screenshot.png" width="80%" alt="Screenshot of Lyx">

It looks like this in the end:

![pdfoutput](<./lyxtemplate_letter_pdfresult_screenshot.png> "Screenshot of the final result, pdf")
